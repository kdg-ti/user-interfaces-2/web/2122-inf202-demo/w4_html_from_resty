export function deleteCar(id) {
  console.log("deleting car " + id);
  return fetch(CARS_URL+id,{method:"delete"}).then(resp => console.log(resp.ok?"succeeded":"failed"))
  
}

const BASE_URL = "http://localhost:3000/";
const PERSONS_URL=BASE_URL + "persons/"
const CARS_URL=BASE_URL + "cars/"

export function getCars(){
  return fetch(CARS_URL).then(resp => resp.json())
}

export function getCar(id){
  return fetch(CARS_URL+id).then(resp => resp.json())
}

export function getCarOwner(id){
  return getCar(id)
    .then(resp => fetch(PERSONS_URL+resp.personId))
    .then(owner => owner.json())
}

export function getCarAndOwner(id) {
  let car
  return getCar(id)
    .then(resp => {
      car = resp;
      return fetch(PERSONS_URL + resp.personId);
    })
    .then(owner => owner.json())
    .then(person => {
      return {car, person}
    })
}




