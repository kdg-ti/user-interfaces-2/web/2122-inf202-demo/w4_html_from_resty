import * as rest from "./rest.js"
const carsNode= document.getElementById("cars");

export function showCarsHtmlSimple(cars) {
  const carNodes=cars.map(car => createNodeWithText("li",`${car.brand} ${car.model} ${car.license}`))
  const ul = document.createElement("ul")
  ul.append(...carNodes)
  carsNode.append(ul)
}

function createNodeWithText(node,text){
  const li = document.createElement(node);
  const textNode=document.createTextNode(text);
  li.append(textNode)
  return li
}

//carsNode.addEventListener("click" , event => rest.deleteCar(event.target.dataset.carId))

export function showCarsHtml(cars) {
  const carNodes=cars.map(createCarWithButton)
  const ul = document.createElement("ul")
  ul.append(...carNodes)
  carsNode.replaceChildren(ul)
}

function createCarWithButton(car) {
  const li= createNodeWithText("li", `${car.brand} ${car.model} ${car.license}`)
  const button = createNodeWithText("button","Delete");
  button.addEventListener("click",event => rest.deleteCar(car.id))
  li.append(button);
  return li
}
// export function showCarsHtmlEvent(cars) {
//   return carsNode.innerHTML =
//     cars.reduce((html,car) => `${html}
//       <li data-car-id="${car.id}"> ${car.brand} ${car.model} ${car.license} </li>`,
//       "<ul>")
//     + "</ul>"
// }
