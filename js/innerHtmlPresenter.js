import * as rest from "./rest.js"

export function showCarsHtmlSimple(cars) {
  return document.getElementById("cars").innerHTML =
    cars.reduce((html,car) => `${html}
      <li> ${car.brand} ${car.model} ${car.license} </li>`,
      "<ul>")
    + "</ul>"
}

const carsNode= document.getElementById("cars");
carsNode.addEventListener("click" , event => rest.deleteCar(event.target.dataset.carId))


export function showCarsHtml(cars) {
  return carsNode.innerHTML =
    cars.reduce((html,car) => `${html}
      <li data-car-id="${car.id}"> ${car.brand} ${car.model} ${car.license} </li>`,
      "<ul>")
    + "</ul>"
}
